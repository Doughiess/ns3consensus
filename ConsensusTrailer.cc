/*
 * Copyright (C) 2018 Jordan Hedges
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/* 
 * File:   ConsensusTrailer.cc
 * Author: Jordan Hedges
 * 
 * Created on 14 September 2018, 3:55 PM
 */

#include "ConsensusTrailer.h"

ConsensusTrailer::ConsensusTrailer(){}

//TODO redo so it works with a vector
ConsensusTrailer::ConsensusTrailer(const std::vector<uint64_t> data, const uint64_t votedSize, 
		const uint64_t acceptedSize, const uint32_t newNodeId ) {
	m_data = data;
	this->votedSize = votedSize;
	this->acceptedSize = acceptedSize;
	this->nodeId = newNodeId;
	//std::cout << "Initialised with size: " << vectorSize << "\n";
}

ConsensusTrailer::ConsensusTrailer(const uint64_t votedSize, const uint64_t acceptedSize)
{

	this->votedSize = votedSize;
	this->acceptedSize = acceptedSize;
}

ConsensusTrailer::ConsensusTrailer(const ConsensusTrailer& orig) {

}

ConsensusTrailer::~ConsensusTrailer(void) {

}

void ConsensusTrailer::Print(std::ostream &os) const {
	for(auto & data : m_data)
	{
		os << "data: " << data;
	}
}

void ConsensusTrailer::Serialize(Buffer::Iterator end) const{
	//std::cerr << "writing size\n";
	Buffer::Iterator newIt = end;
	newIt.Prev(GetSerializedSize());
	newIt.WriteHtonU64(votedSize);
	newIt.WriteHtonU64(acceptedSize);
	//std::cerr << "wrote size\n";
	std::cout << "serialized: " << votedSize << " " << acceptedSize;
	for(uint64_t i = 0; i < votedSize; i++)
	{
		newIt.WriteHtonU64(m_data[i]);
		std::cout << " " << m_data[i];
	}
	for(uint64_t i = votedSize; i < votedSize + acceptedSize; i++)
	{
		newIt.WriteHtonU64(m_data[i]);
		std::cout << " " << m_data[i];
	}
	std::cout << " " << this->nodeId << " " << GetSerializedSize() << "\n";
	newIt.WriteHtonU32(this->nodeId);
	newIt.WriteHtonU32(GetSerializedSize());
	//std::cerr << "checksum: " << start.CalculateIpChecksum(GetSerializedSize()) << "\n";
}

//TODO is read off backwards?
uint32_t ConsensusTrailer::Deserialize(Buffer::Iterator end) {
	//std::cerr << "Deserialize" << "\n";
	Buffer::Iterator newIt = end;
	newIt.Prev(4);
	uint32_t serializedSize = newIt.ReadNtohU32();
	newIt.Prev(serializedSize);
	uint64_t incomingVectorVoted = newIt.ReadNtohU64();
	uint64_t incomingVectorAccepted = newIt.ReadNtohU64();
	std::vector<uint64_t> deserializeVector(incomingVectorVoted + incomingVectorAccepted);
	for(uint64_t i = 0; i < incomingVectorVoted; i++)
	{
		//std::cerr << "Vals ";
		deserializeVector[i] = newIt.ReadNtohU64();
		std::cout << "deserialize: " << deserializeVector[i] << " ";
	}
	std::cout << "\n";
	for(uint64_t i = incomingVectorVoted; i < incomingVectorVoted + incomingVectorAccepted; i++)
	{
		deserializeVector[i] = newIt.ReadNtohU64();
	}
	m_data = deserializeVector;
	votedSize = incomingVectorVoted;
	acceptedSize = incomingVectorAccepted;
	this->nodeId = newIt.ReadNtohU32();
	uint32_t readSize = serializedSize;

	return readSize;
}

uint32_t ConsensusTrailer::GetSerializedSize(void) const 
{
	//std::cout << "GetSerSize" << "\n";
	//std::cout << vectorSize << "\n";	
	uint32_t dataSize = sizeof(uint64_t) + sizeof(uint64_t) + votedSize * sizeof(uint64_t) + 
			acceptedSize * sizeof(uint64_t) + sizeof(uint32_t) + sizeof(uint32_t);
	//std::cout << dataSize << "\n";
	return dataSize;
}

void ConsensusTrailer::SetData(const std::vector<uint64_t> newData)
{
	m_data = newData;
	votedSize = newData.size();
}

std::vector<uint64_t> ConsensusTrailer::GetData(void)
{
	return m_data;
}

TypeId ConsensusTrailer::GetTypeId(void) const
{
	static TypeId tid = TypeId("ConsensusTrailer")
	.SetParent<Header> ()
	.SetGroupName("Network")
	.AddConstructor<ConsensusTrailer>()
	;
	
	return tid;
}

TypeId ConsensusTrailer::GetInstanceTypeId (void) const
{
	return GetTypeId();
}

uint32_t ConsensusTrailer::GetSenderNodeId(void)
{
	return nodeId;
}

uint64_t ConsensusTrailer::GetVotedSize(void)
{
	return votedSize;
}