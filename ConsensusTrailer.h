/*
 * Copyright (C) 2018 Jordan Hedges
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/* 
 * File:   ConsensusTrailer.h
 * Author: Jordan Hedges
 *
 * Created on 14 September 2018, 3:55 PM
 */

#ifndef CONSENSUSTRAILER_H
#define CONSENSUSTRAILER_H

#include "ns3/ptr.h"
#include "ns3/packet.h"
#include "ns3/header.h"
#include <stdint.h>
#include <vector>

using namespace ns3;

class ConsensusTrailer : public Trailer {
public:
    ConsensusTrailer();
	ConsensusTrailer(uint64_t, uint64_t);
	ConsensusTrailer(std::vector<uint64_t>, uint64_t, uint64_t, uint32_t);
	ConsensusTrailer(const ConsensusTrailer& orig);
	virtual ~ConsensusTrailer(void);
	void SetData(std::vector<uint64_t>);
	std::vector<uint64_t> GetData(void);
	virtual void Print(std::ostream &) const;
	virtual void Serialize(Buffer::Iterator) const;
	virtual uint32_t Deserialize(Buffer::Iterator);
	virtual uint32_t GetSerializedSize(void) const;
    virtual TypeId GetInstanceTypeId (void) const;
    virtual TypeId GetTypeId(void) const;
	uint32_t GetSenderNodeId(void);
	uint64_t GetVotedSize(void);

private:
    uint64_t votedSize = 0;
	uint64_t acceptedSize = 0;
	std::vector<uint64_t> m_data;
	uint32_t nodeId;
};

#endif /* CONSENSUSTRAILER_H */

