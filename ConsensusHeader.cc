/*
 * Copyright (C) 2018 Jordan Hedges
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/* 
 * File:   ConsensusHeader.cc
 * Author: Jordan Hedges
 * 
 * Created on 14 September 2018, 1:42 PM
 */

#include <sstream>

#include "ConsensusHeader.h"

ConsensusHeader::ConsensusHeader(){}

ConsensusHeader::ConsensusHeader(const ConsensusPhase newConsensusPhase) {
	packetType = newConsensusPhase;
}

ConsensusHeader::ConsensusHeader(const ConsensusHeader& orig) {
	
}

ConsensusHeader::~ConsensusHeader() {

}

void ConsensusHeader::Serialize(Buffer::Iterator start) const {
	Buffer::Iterator it = start;
	//std::cerr << "writing header\n";
	it.WriteHtonU16( (uint16_t) packetType);
}

uint32_t ConsensusHeader::Deserialize(Buffer::Iterator start) {
	Buffer::Iterator it = start;

	packetType = static_cast<ConsensusPhase>(it.ReadNtohU16());

	return GetSerializedSize();
}

void ConsensusHeader::Print(std::ostream &os) const {
	os << "data=" << (uint16_t) packetType;
}

uint32_t ConsensusHeader::GetSerializedSize(void) const{
	uint32_t dataSize = sizeof (uint16_t); //+ sizeof(uint32_t);

	return dataSize;
}

TypeId ConsensusHeader::GetTypeId(void) const
{
	//TODO make this first TypeId fn correct
	static TypeId tid = TypeId("ConsensusHeader")
	.SetParent<Header> ()
	.SetGroupName("Network")
	.AddConstructor<ConsensusHeader>()
	;
	
	return tid;
}

TypeId ConsensusHeader::GetInstanceTypeId (void) const{
	return GetTypeId();
}

void ConsensusHeader::SetData(ConsensusPhase newData) {
	packetType = newData;
}

ConsensusPhase ConsensusHeader::GetData(void) {
	return packetType;
}

