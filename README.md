# ns3Consensus

A half done implementation of the Stellar consensus protocol for use in the ns-3 vehicle simulator.

# Requirements
Requires libgcrypt, GMP (GNU MP) and ns-3 to be installed.

# Setup
1. Clone into ns-3 scratch directory
2. Edit wscript to add LINKFLAGS to link properly with libgcrypt and GMP
3. Run using waf