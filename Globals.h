/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   Globals.h
 * Author: Jordan Hedges
 *
 * Created on 1 October 2018, 1:14 PM
 */
#include <gmpxx.h>

#ifndef GLOBALS_H
#define GLOBALS_H

enum class ConsensusPhase : uint16_t {
	CONSENSUS_PREPARE, CONSENSUS_CONFIRM, CONSENSUS_EXTERNALISE, CONSENSUS_ERROR, CONSENSUS_NOMINATE
};

const mpz_class SHA_MAX("115792089237316195423570985008687907853269984665640564039457584007913129639936");

#endif /* GLOBALS_H */

