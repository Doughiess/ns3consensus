/* -*- Mode:C++; c-file-style:"gnu"; indent-tabs-mode:nil; -*- */
/*
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation;
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

#include "ns3/core-module.h"
#include "ns3/point-to-point-module.h"
#include "ns3/network-module.h"
#include "ns3/applications-module.h"
#include "ns3/wifi-module.h"
#include "ns3/mobility-module.h"
#include "ns3/csma-module.h"
#include "ns3/internet-module.h"
#include "AppCons.h"

// Default Network Topology
//
//   Wifi 10.1.3.0
//                 AP
//  *    *    *    *
//  |    |    |    |    10.1.1.0
// n5   n6   n7   n0 -------------- n1   n2   n3   n4
//                   point-to-point  |    |    |    |
//                                   ================
//                                     LAN 10.1.2.0

//Topology
//* * *
//| | |
//n0n1n2

using namespace ns3;

NS_LOG_COMPONENT_DEFINE("ThirdScriptExample");

void
CourseChange(std::string context, Ptr<const MobilityModel> model) {
	Vector position = model->GetPosition();
	NS_LOG_UNCOND(context << " x = " << position.x << ", y = " << position.y);
}

int
main(int argc, char *argv[]) {
	bool verbose = true;
	uint32_t nWifi = 2;
	bool tracing = true;
	uint32_t broadcastPort = 8000;

	CommandLine cmd;
	cmd.AddValue("nWifi", "Number of wifi STA devices", nWifi);
	cmd.AddValue("verbose", "Tell echo applications to log if true", verbose);
	cmd.AddValue("tracing", "Enable pcap tracing", tracing);

	cmd.Parse(argc, argv);

	// The underlying restriction of 18 is due to the grid position
	// allocator's configuration; the grid layout will exceed the
	// bounding box if more than 18 nodes are provided.
	if (nWifi > 18) {
		std::cout << "nWifi should be 18 or less; otherwise grid layout exceeds the bounding box" << std::endl;
		return 1;
	}

	if (verbose) {
		LogComponentEnable("UdpEchoClientApplication", LOG_LEVEL_INFO);
		LogComponentEnable("UdpEchoServerApplication", LOG_LEVEL_INFO);
	}

	NodeContainer wifiStaNodes;
	wifiStaNodes.Create(nWifi);

	YansWifiChannelHelper channel = YansWifiChannelHelper::Default();
	YansWifiPhyHelper phy = YansWifiPhyHelper::Default();
	phy.SetChannel(channel.Create());

	WifiHelper wifi;
	//wifi.EnableLogComponents();
	wifi.SetRemoteStationManager("ns3::AarfWifiManager");

	WifiMacHelper mac;
	Ssid ssid = Ssid("ns-3-ssid");
	mac.SetType("ns3::AdhocWifiMac");

	NetDeviceContainer staDevices;
	staDevices = wifi.Install(phy, mac, wifiStaNodes);

	mac.SetType("ns3::ApWifiMac",
			"Ssid", SsidValue(ssid));

	MobilityHelper mobility;

	mobility.SetPositionAllocator("ns3::GridPositionAllocator",
			"MinX", DoubleValue(0.0),
			"MinY", DoubleValue(0.0),
			"DeltaX", DoubleValue(5.0),
			"DeltaY", DoubleValue(10.0),
			"GridWidth", UintegerValue(3),
			"LayoutType", StringValue("RowFirst"));

	mobility.SetMobilityModel("ns3::RandomWalk2dMobilityModel",
			"Bounds", RectangleValue(Rectangle(-50, 50, -50, 50)));
	mobility.Install(wifiStaNodes);


	InternetStackHelper stack;
	stack.Install(wifiStaNodes);

	Ipv4AddressHelper address;

	address.SetBase("10.1.3.0", "255.255.255.0");
	address.Assign(staDevices);
	Ipv4GlobalRoutingHelper::PopulateRoutingTables();

	Simulator::Stop(Seconds(10.0));
	LogComponentEnable("Packet", LOG_LEVEL_INFO);

	for (uint32_t i = 0; i < nWifi; i++) {
		std::ostringstream oss;
		oss << "/NodeList/" << wifiStaNodes.Get(i)->GetId() << "/$ns3::MobilityModel/CourseChange";
		Config::Connect(oss.str(), MakeCallback(&CourseChange));

	}
	//create broadcast socket address
	static const ns3::InetSocketAddress broadcastSocketAddr = ns3::InetSocketAddress(ns3::Ipv4Address("255.255.255.255"),
			broadcastPort);
	//Create broadcast sinks
	for (uint32_t n = 0; n < nWifi; n++) {
		Ptr<ns3::Socket> broadcastSink = ns3::Socket::CreateSocket(NodeList::GetNode(n), UdpSocketFactory::GetTypeId());
		broadcastSink->SetAllowBroadcast(true);
		InetSocketAddress beacon_local =
				ns3::InetSocketAddress(Ipv4Address::GetAny(), broadcastPort);
		if(broadcastSink->Bind(beacon_local) == -1)
		{
			std::cerr << "bind failed\n";
		}
		Ptr<ConsensusApp> consensusApp = CreateObject<ConsensusApp, uint32_t>(NodeList::GetNode(n)->GetId());
		Ptr<ns3::Socket> broadcastSource =
				Socket::CreateSocket(NodeList::GetNode(n), UdpSocketFactory::GetTypeId());
		broadcastSource->Connect(broadcastSocketAddr);
		broadcastSource->SetAllowBroadcast(true);
		consensusApp->SetupBroadcastSocket(broadcastSink, broadcastSource);
		NodeList::GetNode(n)->AddApplication(consensusApp);
		//Create broadcast source
		std::cerr << "Adding application for: " << NodeList::GetNode(n) << "\n"; 
	}
	phy.EnablePcap("out.dump", staDevices, true);
	stack.EnablePcapIpv4("ipdump", wifiStaNodes);
	Simulator::Run();
	Simulator::Destroy();
	return 0;
}
