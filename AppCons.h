/*
 * Copyright (C) 2018 Jordan Hedges
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/* 
 * File:   AppCons.h
 * Author: Jordan Hedges
 *
 * Created on 18 September 2018, 11:16 AM
 */

#ifndef APPCONS_H
#define APPCONS_H

#include "ns3/core-module.h"
#include "ns3/network-module.h"
#include "ns3/csma-module.h"
#include "ns3/internet-module.h"
#include "ns3/point-to-point-module.h"
#include "ns3/applications-module.h"
#include "ns3/ipv4-global-routing-helper.h"
#include "ns3/type-id.h"
#include "ConsensusHeader.h"
#include "ConsensusTrailer.h"
#include "Globals.h"
#include <set>
#include <stdint.h>
#include <gcrypt.h>
#include <gmpxx.h>

using namespace ns3;

class ConsensusApp : public Application {
public:
	ConsensusApp();
	ConsensusApp(uint32_t);
	~ConsensusApp();

	/**
	 * Register this type.
	 * \return The TypeId.
	 */
	static TypeId GetTypeId(void);
	void SetupBroadcastSocket(Ptr<Socket>, Ptr<Socket>);
	std::set<TypeId> GetConsensusSlice(void);
	void SetConsensusSlice(std::set<TypeId>);
	std::set<TypeId> InitConsensusSlice(void);
	Ptr<Packet> ExternaliseChoice(void);
	Ptr<Packet> VoteOnValue(void);
    bool ProcessConsensusPacket(Ptr<Packet>);

private:
	virtual void StartApplication(void);
	virtual void StopApplication(void);

	void ScheduleTx(Ptr<Packet>);
	void SendPacket(Ptr<Packet> );
    void ProcessConsensusPrepare(ConsensusPhase, ConsensusTrailer);
    void ProcessConsensusConfirm(ConsensusPhase, ConsensusTrailer);
    void ProcessConsensusExternalise(ConsensusPhase, ConsensusTrailer);
	void ProcessConsensusNominate(ConsensusPhase, ConsensusTrailer);
    bool CheckIfNeighbour(TypeId);
    void AssignPriority(uint32_t);
    std::string SHA256(char*);
    bool ValidityFunction(uint64_t);
    std::string HashConsensusSlice(void);
    void ReceivePacket(Ptr<Socket>);
    void IterateConsensus(void);

	Ptr<Socket> m_socketRecv;
    Ptr<Socket> m_socket;
	Address m_peer;
	DataRate m_dataRate;
	EventId m_sendEvent;
	bool m_running;
	uint32_t m_packetsSent;
	std::set<TypeId> consensusSlice;
    std::set<TypeId> heardFrom;
    std::set<TypeId> neighbours;
	std::map<Packet, int> ongoingVotes; //TODO change value type to something that makes sense
	std::map<uint32_t, Ptr<Packet> > lastReceived;
    std::map<uint32_t, mpz_class> nodePriorities;
    std::vector<std::vector<uint64_t> > receivedPacketBuffer;
    ConsensusPhase currentPhase;
    std::vector<uint64_t> currentCandidate;
    std::vector<uint64_t> nominatedValues;
    std::vector<uint64_t> acceptedNominatedValues;
    uint32_t bestKnownId = 0;
    mpz_class bestKnownPriority = -1;
    double CalculateNeighbourWeight(TypeId);
    void CalculateNeighbourPriority(TypeId);
    int roundNumber = 0;
    //Threshold for proposed input to be considered correct, completely arbitrary.
    uint64_t threshold = 10;
    int m_packetsReceived = 0;
	bool oneAccepted = false;
	uint32_t nodeId;
};

#endif /* APPCONS_H */

