/*
 * Copyright (C) 2018 Jordan Hedges
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/* 
 * File:   ConsensusHeader.h
 * Author: Jordan Hedges
 *
 * Created on 14 September 2018, 1:42 PM
 */

#ifndef CONSENSUSHEADER_H
#define CONSENSUSHEADER_H

#include <stdint.h>
#include "ns3/core-module.h"
#include "ns3/header.h"
#include "AppCons.h"
#include "Globals.h"

using namespace ns3;

class ConsensusHeader : public Header {
public:
    ConsensusHeader();
	ConsensusHeader(const ConsensusPhase);
	virtual void Serialize(Buffer::Iterator) const;
	virtual uint32_t Deserialize(Buffer::Iterator);
	virtual uint32_t GetSerializedSize(void) const;
	virtual void Print(std::ostream &) const;
	ConsensusHeader(const ConsensusHeader&);
	virtual ~ConsensusHeader();
    virtual TypeId GetInstanceTypeId (void) const;
    virtual TypeId GetTypeId(void) const;
    
	ConsensusPhase GetData(void);
	void SetData(ConsensusPhase);
private:
	ConsensusPhase packetType = ConsensusPhase::CONSENSUS_ERROR;
};

#endif /* CONSENSUSHEADER_H */

