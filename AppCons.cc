#include <set>
#include "ns3/core-module.h"
#include "ns3/network-module.h"
#include "ns3/csma-module.h"
#include "ns3/internet-module.h"
#include "ns3/point-to-point-module.h"
#include "ns3/applications-module.h"
#include "ns3/ipv4-global-routing-helper.h"
#include "ns3/type-id.h"
#include "AppCons.h"
#include <map>
#include <iostream>
#include <sstream>
#include <string>
#include <iomanip>
#include "SHA.h"
#include <gmpxx.h>
#include <stdio.h>
#include <random>


using namespace ns3;

ConsensusApp::ConsensusApp()
: m_socket(0),
m_peer(),
m_dataRate(1000.0),
m_sendEvent(),
m_running(false),
m_packetsSent(0) {
	currentPhase = ConsensusPhase::CONSENSUS_NOMINATE;
}

ConsensusApp::ConsensusApp(uint32_t installedNodeId)
: m_socket(0),
m_peer(),
m_dataRate(1000.0),
m_sendEvent(),
m_running(false),
m_packetsSent(0) {
	currentPhase = ConsensusPhase::CONSENSUS_NOMINATE;
	nodeId = installedNodeId;
}

ConsensusApp::~ConsensusApp() {
	m_socket = 0;
}

/* static */
TypeId ConsensusApp::GetTypeId(void) {
	static TypeId tid = TypeId("ConsensusApp")
			.SetParent<Application> ()
			.SetGroupName("Consensus")
			.AddConstructor<ConsensusApp> ()
			;
	return tid;
}

void
ConsensusApp::SetupBroadcastSocket(Ptr<Socket> receiveSocket, Ptr<Socket> broadcastSocket) {
	this->m_socketRecv = receiveSocket;
	this->m_socketRecv->SetRecvCallback(MakeCallback(&ConsensusApp::ReceivePacket, this));
	this->m_socket = broadcastSocket;
	Simulator::Schedule(Seconds(0.1), &ConsensusApp::IterateConsensus, this);
}

void
ConsensusApp::StartApplication(void) {
	m_running = true;
	m_packetsSent = 0;
	//m_socket->Bind();
	//m_socket->Connect(m_peer);
	//SendPacket();
	//Time delay,  Class method function signature, type of class object
	this->m_sendEvent = Simulator::Schedule(Seconds(0.0), &ConsensusApp::IterateConsensus, this);
}

void
ConsensusApp::StopApplication(void) {
	m_running = false;

	if (m_sendEvent.IsRunning()) {
		Simulator::Cancel(m_sendEvent);
	}

	if (m_socket) {
		m_socket->Close();
	}
}

//Assigns a priority (numerical hash) to a given sender and updates class members accordingly
void
ConsensusApp::AssignPriority(uint32_t sender) {
	//Concat "2", round number and then the thing representing the neighbour(typeID?) and hash
	std::ostringstream oss;
	oss << "2" << this->roundNumber << sender << "abcd";
	std::string concatString = oss.str();
	std::vector<char> writable(concatString.begin(), concatString.end());
	writable.push_back('\0');

	char *cCatString = &writable[0];
	std::string hashString = SHA256(cCatString);
	mpz_class hashNum(hashString, 16);
	this->nodePriorities[sender] = hashNum;
	if (hashNum > this->bestKnownPriority) {
		std::cout << "new highest: " << hashNum << " from: " << this->bestKnownId << "\n";
		this->bestKnownPriority = hashNum;
		this->bestKnownId = sender;
	}
}

std::string
ConsensusApp::HashConsensusSlice(void)
{
	std::ostringstream oss;
	for(auto &nodes : this->consensusSlice)
	{
		oss << nodes << " ";
	}
	std::string concatSlice = oss.str();
	std::vector<char> writable(concatSlice.begin(), concatSlice.end());
	writable.push_back('\0');
	
	char *cCatString = &writable[0];
	std::string returnString = SHA256(cCatString);
	return returnString;
}



std::string
ConsensusApp::SHA256(char* inputString) {
	unsigned char *hashResult;
	unsigned i;
	unsigned int l = gcry_md_get_algo_dlen(GCRY_MD_SHA256); /* get digest length (used later to print the result) */

	gcry_md_hd_t h;
	gcry_md_open(&h, GCRY_MD_SHA256, GCRY_MD_FLAG_SECURE); /* initialise the hash context */
	gcry_md_write(h, inputString, strlen(inputString)); /* hash some text */
	hashResult = gcry_md_read(h, GCRY_MD_SHA256); /* get the result */
	char* buffer = static_cast<char*> (calloc(l * 2 + 1, 1));
	char hex[3];
	for (i = 0; i < l; i++) {
		snprintf(hex, 3, "%02x", hashResult[i]); /* print the result */
		memcpy(buffer + (i * 2), hex, 2);
	}
	buffer[l * 2] = '\0';
	std::string returnString(buffer);
	free(buffer);
	gcry_md_close(h);
	return returnString;
}

bool
ConsensusApp::CheckIfNeighbour(TypeId nodeTID) {
	//Concat "1", round number and then the thing representing the neighbour(typeID?) and hash
	//Determine if each nodes hash is less than the maximum of SHA-256 (2^256) multiplied by their weight from
	// calculate neighbour weight.
	double neighbourWeight = CalculateNeighbourWeight(nodeTID);
	std::ostringstream oss;
	oss << "1" << this->roundNumber << nodeTID;
	std::string concatString = oss.str();
	std::vector<char> writable(concatString.begin(), concatString.end());
	writable.push_back('\0');

	char *cCatString = &writable[0];
	std::string hashString = SHA256(cCatString);
	mpz_class hashResult(hashString, 16);
	if (hashResult < SHA_MAX * neighbourWeight) {
		return true;
	} else return false;
}

//Get consensus slice for this node, find fraction of slices containing given neighbour

double ConsensusApp::CalculateNeighbourWeight(TypeId nodeTID) {
	for (auto &sliceNodeTID : this->consensusSlice) {
		if (nodeTID == sliceNodeTID) {
			return 1.0;
		}
	}
	return 0.0;
}

void
ConsensusApp::SendPacket(Ptr<Packet> transmittedPacket) {
	m_socket->Send(transmittedPacket);
	//std::cerr << m_socket->Send(0, 8, 0) << "\n";
	//transmittedPacket->Print(std::cout);
	//std::cerr << m_socket->SendTo(0, 8, 0, ns3::InetSocketAddress(ns3::Ipv4Address("255.255.255.255"), 8000)) << "\n";
}

void
ConsensusApp::ScheduleTx(Ptr<Packet> transmittedPacket) {
	if (m_running) {
		Time tNow(Seconds(0.0));
		Time tNext(Seconds(1 * 8 / static_cast<double> (m_dataRate.GetBitRate())));
		std::cerr << tNext << " " << transmittedPacket->GetSize() << " " << m_dataRate.GetBitRate() << "\n";
		m_sendEvent = Simulator::Schedule(tNow, &ConsensusApp::SendPacket, this, transmittedPacket);
	}
}



std::set<TypeId>
ConsensusApp::GetConsensusSlice(void) {
	return this->consensusSlice;
}

void
ConsensusApp::SetConsensusSlice(std::set<TypeId> newSlice) {
	this->consensusSlice = newSlice;
}

std::set<TypeId>
ConsensusApp::InitConsensusSlice(void) {
	std::set<TypeId> consensusSlice;
	return consensusSlice;
}

Ptr<Packet>
ConsensusApp::VoteOnValue(void) {
	Ptr<Packet> packet = Create<Packet>();
	//ConsensusHeader voteHeader = ConsensusHeader(ConsensusPhase::CONSENSUS_CONFIRM);
	//packet->AddHeader(voteHeader);
	return packet;
}

Ptr<Packet>
ConsensusApp::ExternaliseChoice(void) {
	Ptr<Packet> packet = Create<Packet>();
	//ConsensusHeader externaliseHeader = ConsensusHeader(ConsensusPhase::CONSENSUS_EXTERNALISE);
	//packet->AddHeader(externaliseHeader);
	ConsensusTrailer externaliseTrailer = ConsensusTrailer();
	packet->AddTrailer(externaliseTrailer);
	return packet;

}

void ConsensusApp::ProcessConsensusPrepare(ConsensusPhase currentApplicationPhase, ConsensusTrailer data) {
	if (currentApplicationPhase == ConsensusPhase::CONSENSUS_PREPARE) {
		//do something
	} else {
		//do nothing?
	}
}

void ConsensusApp::ProcessConsensusConfirm(ConsensusPhase currentApplicationPhase, ConsensusTrailer data) {
	if (currentApplicationPhase == ConsensusPhase::CONSENSUS_CONFIRM) {
		//do something
	} else {
		//do nothing?
	}
}

void ConsensusApp::ProcessConsensusExternalise(ConsensusPhase currentApplicationPhase, ConsensusTrailer data) {
	if (currentApplicationPhase == ConsensusPhase::CONSENSUS_EXTERNALISE) {

	} else {
		//do nothing?
	}
}

void ConsensusApp::ProcessConsensusNominate(ConsensusPhase currentApplicationPhase, ConsensusTrailer data) {
	if (currentApplicationPhase != ConsensusPhase::CONSENSUS_NOMINATE) {
		this->receivedPacketBuffer.push_back(data.GetData());
	} else {
		if(this->nodePriorities[data.GetSenderNodeId()] >= this->bestKnownPriority)//highest priority
		{
			this->currentCandidate = data.GetData();
			//TODO check voted/accepted size is updated correctly as they go
			if(this->acceptedNominatedValues.size() == 0 || data.GetVotedSize() == 0)
			{
				//check if any of the values are accepted by you, move them to accepted if they are and begin balloting
			}
			else
			{
				//someone has accepted at least one of the nominated values, begin balloting
				this->currentPhase = ConsensusPhase::CONSENSUS_PREPARE;
			}
		}
	}
}

bool
ConsensusApp::ProcessConsensusPacket(Ptr<Packet> consensusPacket) {
	
	ConsensusHeader packetConsensusHeader;
	ConsensusTrailer packetConsensusTrailer;
	
	consensusPacket->RemoveHeader(packetConsensusHeader);
	std::cout << "test: " << consensusPacket->RemoveTrailer(packetConsensusTrailer) << "\n";
	//consensusPacket->RemoveTrailer(packetConsensusTrailer);
	
	std::vector<uint64_t> validValues;
	for(auto & dataValue : packetConsensusTrailer.GetData())
	{
		if(ValidityFunction(dataValue)) 
		{
			validValues.push_back(dataValue);
		}
	}
	packetConsensusTrailer.SetData(validValues);
	Ptr<Packet> validPacket = Create<Packet>();
	validPacket->AddHeader(packetConsensusHeader);
	validPacket->AddTrailer(packetConsensusTrailer);
	this->lastReceived[packetConsensusTrailer.GetSenderNodeId()] = validPacket;
	AssignPriority(packetConsensusTrailer.GetSenderNodeId());
	std::cout << "sender: " << packetConsensusTrailer.GetSenderNodeId() << " receiver: " << this->nodeId << "\n";
	std::cout << "sender priority: " << this->nodePriorities[packetConsensusTrailer.GetSenderNodeId()] << "\n";
	switch (packetConsensusHeader.GetData()) {
		case ConsensusPhase::CONSENSUS_PREPARE:
		{
			ProcessConsensusPrepare(this->currentPhase, packetConsensusTrailer);
			break;
		}
		case ConsensusPhase::CONSENSUS_CONFIRM:
		{
			ProcessConsensusConfirm(this->currentPhase, packetConsensusTrailer);
			break;
		}
		case ConsensusPhase::CONSENSUS_EXTERNALISE:
		{
			ProcessConsensusExternalise(this->currentPhase, packetConsensusTrailer);
			break;
		}
		case ConsensusPhase::CONSENSUS_NOMINATE:
		{
			ProcessConsensusNominate(this->currentPhase, packetConsensusTrailer);
		}
		default:
			break;
	}
	return true;
}

//Application level sanity checks on proposed values, just do less than of some number for now?
bool
ConsensusApp::ValidityFunction(uint64_t proposedValue){
	bool valid = proposedValue < this->threshold ? true : false;
	return valid;
}

void
ConsensusApp::ReceivePacket(Ptr<Socket> receiveSocket)
{
	Ptr<Packet> receivedPacket = receiveSocket->Recv();
	//std::cerr << "recvd ";
	ProcessConsensusPacket(receivedPacket);
}

void
ConsensusApp::IterateConsensus(void)
{
	//do state based action depending on phase and round no.
	//3 options, new to group, you are highest priority, someone else is highest priority
	//if new, query for info, you are highest, choose something to add to list, someone else is highest, echo them
	//transmit a packet based on this
	//after, re add this function to the event queue?
	//std::cout << "Start iterating\n";
	//AssignPriority(this->nodeId);
	for(auto & kv : this->lastReceived)
	{
		std::cout << "map " << kv.first << " "  << " " << this->nodeId << "\n";
		kv.second->Print(std::cerr);
	}
	//someone else highest priority
	if(this->lastReceived.count(this->bestKnownId) != 0)
	{
		//echo best we have heard so far
		//TODO update from nominated to accepted based on quorum
		Ptr<Packet> echoPacket = this->lastReceived[this->bestKnownId];
		std::cout << echoPacket->GetSize() << "\n";
		ConsensusTrailer echoPacketTrailer;
		echoPacket->PeekTrailer(echoPacketTrailer);
		for(auto & data : echoPacketTrailer.GetData())
		{
			std::cout << "data: " << data << "\n";
		}
		this->ScheduleTx(echoPacket);
		this->bestKnownPriority = -1;
		this->bestKnownId = 0;
		std::cout << "echoing\n";
	}
	else //you are highest priority or new to group
	{
		//TODO change to something that represents real data?
		Ptr<Packet> createdPacket = Create<Packet>();
		ConsensusHeader packetHeader(ConsensusPhase::CONSENSUS_NOMINATE);
		
		std::mt19937 generator;
		generator.seed(std::random_device()());
		std::uniform_int_distribution<std::mt19937::result_type> dist12(0, 12);
		
		uint64_t randNum = dist12(generator);
		std::vector<uint64_t> packetData = {randNum};
		ConsensusTrailer packetTrailer(packetData, 1, 0, this->nodeId);
		createdPacket->AddHeader(packetHeader);
		createdPacket->AddTrailer(packetTrailer);
		
		this->ScheduleTx(createdPacket);
		this->bestKnownPriority = -1;
		this->bestKnownId = 0;
		//this->lastReceived.clear();
	}
	std::cout << "Iterating\n";
	Simulator::Schedule(Seconds(0.1), &ConsensusApp::IterateConsensus,  this);
	this->roundNumber++;
	AssignPriority(this->nodeId);
}